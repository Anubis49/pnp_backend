package handler

import (
	"encoding/json"
	"github.com/go-playground/validator/v10"
	"log"
	"net/http"
)

type Item struct {
	Id          string  `json:"id"`
	Name        string  `json:"name" validate:"required"`
	Url         string  `json:"url"`
	Description string  `json:"description"`
	Weight      float32 `json:"weight"`
	Price       float32 `json:"price"`
	Size        float32 `json:"size"`
}

func (i *Item) Validate() error {
	validate := validator.New()
	return validate.Struct(i)
}

type ItemService interface {
	Get() ([]Item, error)
	Upsert(item Item) error
	Delete(item Item) error
}

type ItemHandler struct {
	item    Item
	ItemSvc ItemService
}

func New(svc ItemService) ItemHandler {
	return ItemHandler{
		ItemSvc: svc,
	}
}

func (itemHdl *ItemHandler) serilizeItem(w http.ResponseWriter, r *http.Request) error {
	item := Item{}
	err := json.NewDecoder(r.Body).Decode(&item)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return err
	}
	itemHdl.item = item
	return nil
}

func (itemHdl *ItemHandler) GetItems(w http.ResponseWriter, r *http.Request) {
	var err error
	var items []Item
	items, err = itemHdl.ItemSvc.Get()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	jsonBytes, err := json.Marshal(items)
	if err != nil {
		log.Fatal(err)
	}
	w.Write(jsonBytes)
}

func (itemHdl *ItemHandler) PatchItem(w http.ResponseWriter, r *http.Request) {
	// serilize
	if err := itemHdl.serilizeItem(w, r); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// validate Items
	if err := itemHdl.item.Validate(); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Service Patch Service
	if err := itemHdl.ItemSvc.Upsert(itemHdl.item); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (itemHdl *ItemHandler) DeleteItem(w http.ResponseWriter, r *http.Request) {
	// serilize
	if err := itemHdl.serilizeItem(w, r); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// validate Items
	if err := itemHdl.item.Validate(); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// checkForID
	if itemHdl.item.Id == "" {
		http.Error(w, "missing ItemID", http.StatusBadRequest)
		return
	}

	// Delete
	if err := itemHdl.ItemSvc.Delete(itemHdl.item); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
