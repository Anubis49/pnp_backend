package handler_test

import (
	"bytes"
	"encoding/json"
	"itemservice/pkg/item/handler"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
)

type ItemsSvc_mock struct {
}

func (u *ItemsSvc_mock) Get() ([]handler.Item, error) {
	return nil, nil
}
func (u *ItemsSvc_mock) Upsert(item handler.Item) error {
	return nil
}
func (u *ItemsSvc_mock) Delete(item handler.Item) error {
	return nil
}

func TestPatchItemsHandler(t *testing.T) {
	item := handler.Item{
		Id:          "",
		Name:        "rawr",
		Url:         "awd",
		Description: "awd",
		Weight:      0,
		Price:       0,
		Size:        0,
	}
	jsonBytes, err := json.Marshal(item)
	if err != nil {
		log.Fatal(err)
	}
	t.Log("ItemService: TestItemHandler: ", string(jsonBytes))
	req, err := http.NewRequest("PATCH", "/item", bytes.NewBuffer(jsonBytes))
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()

	svc := ItemsSvc_mock{}
	hdl := handler.New(&svc)
	handler := http.HandlerFunc(hdl.PatchItem)

	handler.ServeHTTP(rr, req)
	if rr.Code != http.StatusOK {
		t.Fatal(rr.Code)
	}
}

func TestPatchItemsErrHandler(t *testing.T) {
	errItem := handler.Item{
		Id:          "",
		Name:        "",
		Url:         "awd",
		Description: "awd",
		Weight:      0,
		Price:       0,
		Size:        0,
	}
	jsonBytes, err := json.Marshal(errItem)
	if err != nil {
		log.Fatal(err)
	}
	t.Log("ItemService: TestItemHandler: ", string(jsonBytes))
	req, err := http.NewRequest("PATCH", "/item", bytes.NewBuffer(jsonBytes))
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()

	hdl := handler.New(&ItemsSvc_mock{})
	handler := http.HandlerFunc(hdl.PatchItem)

	handler.ServeHTTP(rr, req)
	t.Log("ItemService: TestPatchItemsErrHandler: error code: ", rr.Code)
	if rr.Code != http.StatusBadRequest {
		t.Fatal(rr.Code)
	}
	t.Log(rr.Body.String())
}

func TestDeleteItemsHandler(t *testing.T) {
	errItem := handler.Item{
		Id:          "1",
		Name:        "awdasd",
		Url:         "asd",
		Description: "asd",
		Weight:      0,
		Price:       0,
		Size:        0,
	}
	jsonBytes, err := json.Marshal(errItem)
	if err != nil {
		log.Fatal(err)
	}
	t.Log("ItemService: TestItemHandler: ", string(jsonBytes))
	req, err := http.NewRequest("DELETE", "/item", bytes.NewBuffer(jsonBytes))
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()

	hdl := handler.New(&ItemsSvc_mock{})
	handler := http.HandlerFunc(hdl.DeleteItem)

	handler.ServeHTTP(rr, req)
	t.Log("ItemService: TestDeleteItemsErrHandler: error code: ", rr.Code)
	if rr.Code != http.StatusOK {
		t.Fatal(rr.Code)
	}
	t.Log(rr.Body.String())
}
