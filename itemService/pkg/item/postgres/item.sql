CREATE TABLE items(
      id          SERIAL,
      name        VARCHAR(90) NOT NULL,
      url         VARCHAR(510),
      description VARCHAR(255),
      weight      FLOAT(32),
      price       FLOAT(32),
      size        FLOAT(32),
      PRIMARY KEY (id)
);