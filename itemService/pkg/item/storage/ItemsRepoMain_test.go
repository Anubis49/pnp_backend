package storage_test

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"log"
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	const (
		host     = "127.0.0.1"
		port     = "5432"
		user     = "test"
		password = "test"
		dbname   = "postgres"
	)
	connStr := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec("CREATE DATABASE Itemservice")
	if err != nil {
		log.Panic(err)
	}

	exitVal := m.Run()

	os.Exit(exitVal)
}
