package storage

import (
	"go.mongodb.org/mongo-driver/mongo"
	"itemservice/pkg/item/service"
)

type Item struct {
	db          mongo.Client
	Id          string  `bson:"_id"`
	Name        string  `bson:"name"`
	Url         string  `bson:"url"`
	Description string  `bson:"description"`
	Weight      float32 `bson:"weight"`
	Price       float32 `bson:"price"`
	Size        float32 `bson:"size"`
}

func (i *Item) SetDb(db mongo.Client) {
	i.db = db
}

func (i *Item) marshal(item service.Item) {
	i.Id = item.Id
	i.Name = item.Name
	i.Url = item.Url
	i.Description = item.Description
	i.Weight = item.Weight
	i.Price = item.Price
	i.Size = item.Size
}

func (i *Item) GetItems() ([]Item, error) {
	return []Item{}, nil
}

func (i *Item) Upsert(item service.Item) error {
	return nil
}

func (i *Item) Delete(item service.Item) error {

	return nil
}
