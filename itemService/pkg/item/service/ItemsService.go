package service

import (
	"itemservice/pkg/item/handler"
)

type Item struct {
	Id          string
	Name        string
	Url         string
	Description string
	Weight      float32
	Price       float32
	Size        float32
}

type ItemService struct {
	Item Item
	Repo Repository
}

type Repository interface {
	Upsert(item Item) error
	Delete(item Item) error
}

func (srvI *ItemService) marshal(item handler.Item) {
	srvI.Item = Item{
		Id:          item.Id,
		Name:        item.Name,
		Url:         item.Url,
		Description: item.Description,
		Weight:      item.Weight,
		Price:       item.Price,
		Size:        item.Size,
	}
}

func New(repo Repository) ItemService {
	return ItemService{
		Repo: repo,
	}
}

func (i ItemService) Upsert(item handler.Item) error {
	i.marshal(item)
	i.Repo.Upsert(i.Item)
	return nil
}

func (i ItemService) Delete(item handler.Item) error {
	i.marshal(item)
	i.Repo.Delete(i.Item)
	return nil
}
