package service_test

import (
	"itemservice/pkg/item/handler"
	"itemservice/pkg/item/service"
	"testing"
)

type storageMock struct {
}

func (s storageMock) Upsert(item service.Item) error {
	return nil
}

func (s storageMock) Delete(item service.Item) error {
	return nil
}

func TestUpsertItem(t *testing.T) {
	store := storageMock{}
	srv := service.New(&store)
	item := handler.Item{
		Id:          "",
		Name:        "123",
		Url:         "https://xyz",
		Description: "what ever",
		Weight:      0,
		Price:       0,
		Size:        0,
	}
	if err := srv.Upsert(item); err != nil {
		t.Log(err)
	}
}

func TestDeleteItem(t *testing.T) {
	store := storageMock{}
	srv := service.New(&store)
	item := handler.Item{
		Id:          "0",
		Name:        "123",
		Url:         "https://xyz",
		Description: "what ever",
		Weight:      0,
		Price:       0,
		Size:        0,
	}
	if err := srv.Upsert(item); err != nil {
		t.Log(err)
	}
}
